const axios = require('axios').default;
const { turnioToken } = require("../util/global/config")
const HypertensionSymptoms = require("../util/symptoms/hypertension")

axios.defaults.headers.common['Authorization'] = axios.defaults.headers.common['Authorization'] = turnioToken;

exports.checkHypertension = async (req, res) => {
    console.log(req.body.messages)
    // turn io webhook body request
    let survery = req.body.messages
    let response = await HypertensionSymptoms(req.query.iso, survery)

    axios.post("https://whatsapp.turn.io/v1/messages/",
        {
            "text": {
                "body": `Thank you for answering our questions \n${response}`
            },
            "to": `${req.body.contacts[0].wa_id}`,
            "type": "text"
        }
    )

    res.send("done check hypertension")
}