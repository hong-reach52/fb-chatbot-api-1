const express = require('express');
const routerHouseholdInfo = require('express').Router();
const model = require("../model/datahandler");

var errorInputMessage = {
                          "Eng" : "You have input invalid choices. Please try again",
                          "Hil" : "Sala nga choices ang imo gin-enter. Palihog liwat sang imo sabat."
}

routerHouseholdInfo.post('/household_info', function(req, res) {
    console.log("routerHouseholdInfo post");
    console.log(req.body);
    var lang = req.body.lang.substring(0,3);
    var amenties = req.body.amenties;
    var jsonResponse = {};
    
    if(typeof amenties !== "undefined") {
        if(amenties.split(",").some(isNaN) &&
           amenties.toLowerCase() != "none" &&
           amenties.toLowerCase() != "wala") {
            jsonResponse = {
 				"messages": [
   				{"text": errorInputMessage [lang]}
 			    ],
                                "redirect_to_blocks": [lang + " Household 2"]
			};
        } else {
            jsonResponse = {
                                //"redirect_to_blocks": [lang + " Health Info 4"]
			};
        }
    }
    console.log(jsonResponse);
    res.send(jsonResponse);
});

routerHouseholdInfo.post('/save_household_info', function(req, res) {
    console.log("routerHouseholdInfo post");

    var jsonResponse = {};

    model.save_data("household_info", req.body);

    var jsonResponse = {
 			"messages": [
   					{"text": "Your household info data is saved!"}	
 				]
			};
    console.log(jsonResponse);
    res.send(jsonResponse);
});


module.exports = routerHouseholdInfo;