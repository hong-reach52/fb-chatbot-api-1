const express = require("express");
const router = express.Router();
const { checkHypertension } = require("../controller/hypertension")

router.route("/").post(checkHypertension)

module.exports = router